This module is an integration for the plista RecommendationAds.

plista RecommendationAds are an innovative solution for publishers to generate
additional advertising revenue and traffic, while at the same time increasing
the site’s value for the user. As an additional ad format plista
RecommendationAds are displayed on your website among user-relevant editorial
recommendations (“You might also like”).

RecommendationAds are text/image ads are surrounded by interest-based editorial
recommendations for the relevant websites. Thanks to the position within the
reader’s attention area and the sophisticated targeting,
this advertising space achieves an above-average CTR.

Features of this module:

  Show widget in nodes as field
  Show widget as a block
  Define plista fields through tokens
  Disable widget on specific url's
  Update nodes through plista API

Installation:

Go to admin/config/services/plista and configure the module.
After that there are two possibilities to use this module.
It can be used as a field or as a block.
